<footer class="page-footer font-small indigo mt-5" style="background-image: linear-gradient(to top, #0099cc, #0086b3, #007399);">
  <div class="container text-center text-md-left">
    <div class="row">
      <div class="col-md-3 mx-auto text-center">
         <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Informações Gerais</h5>
		<p class="">Autor: Vitor Oliveira</p>
        <p class="">Prontuário: 180037-x</p>
		 <p class="">Linguagem Orientada a Objetos</p>
	</div>
  </div>
  <div class="footer-copyright text-center py-3">© 2018 Copyright:
    <a href="https://mdbootstrap.com/education/bootstrap/"> Bootstrap.com</a>
  </div>
  </div>
</footer>