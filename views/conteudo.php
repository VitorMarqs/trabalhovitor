<main class="mt-5 pt-5">
    <div class="container">
      <section class="card blue-gradient wow fadeIn" id="intro">
        <div class="card-body text-white text-center py-5 px-5 my-5">
          <h1 class="mb-4">
            <strong>Bem Vindo a VitorMania!</strong>
          </h1>
          <p>
            <strong>Somos Uma microempresa de envio de arquivos, por exemplo, currículos, 
			formulários e páginas de apresentação.</strong>
          </p>
          <p class="mb-4">
            <strong>Este site foi criado com o auxílio do Material Design Bootstrap.</strong>
          </p>
          <a target="_blank" href="https://mdbootstrap.com/education/bootstrap/" class="btn btn-outline-white btn-lg">
		  Quer ver e aprender mais?
            <i class="fas fa-graduation-cap ml-2"></i>
          </a>
        </div>
      </section>
      <section class="pt-5">
        <div class="wow fadeIn">
          <h2 class="h1 text-center mb-5">O que é o VitorMania?</h2>
          <p class="text-center">O VitorMania é um site para você enviar seus documentos e nunca mais se preocupar com eles 😊 </p>
          <p class="text-center mb-5 pb-5">O VitorMania já guardou
		  cerca de mais de <strong>484 037</strong> arquivos das pessoas mais importantes, então pode confiar.</p>
        </div>
        <div class="row wow fadeIn">
         
            <div class="view overlay rounded z-depth-1-half">
              <div class="view overlay">
				<img src="img/sobre.jpg" height="300" width="300">
              </div>
            </div>
         
          <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
            <h3 class="mb-3 font-weight-bold dark-grey-text">
              <strong>Sobre o Autor</strong>
            </h3>
            <p class="grey-text">Eu sou um jovem aluno de informática com um grande futuro pela frente e esse site é apenas uma palhinha do que eu consigo fazer com HTML.</p>
            <p>
              <strong>Espetacular não é? Também acho</strong>
            </p>
            <a href="visualiza/index.php" target="_blank" class="btn btn-primary btn-md">+ Documentos
              <i class="fas fa-play ml-2"></i>
            </a>
          </div>
        </div>
        <hr class="mb-5 mt-3">
        <div class="row mt-3 wow fadeIn">
          <div class="">
            <div class="view overlay rounded z-depth-1">
              <img src="img/arquivo.jpg" class="img-fluid" height="300" width="300">
            </div>
          </div>
          <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
            <h3 class="mb-3 font-weight-bold dark-grey-text">
              <strong>Os Documentos</strong>
            </h3>
            <p class="grey-text">Aqui é o seu lugar de enviar os seus tão importantes documentos.</p>
            <a href="enviar/index.php" target="_blank"
              class="btn btn-primary btn-md">Enviar Documentos
              <i class="fas fa-play ml-2"></i>
            </a>
          </div>
        </div>
        <hr class="mb-5 mt-3">
        <div class="row wow fadeIn">
          <div class="col-lg-5 col-xl-4 mb-4">
            <div class="view overlay rounded z-depth-1">
              <img src="https://mdbootstrap.com/wp-content/uploads/2018/01/push-fb.jpg" class="img-fluid" alt="">
              <a href="https://mdbootstrap.com/education/tech-marketing/web-push-introduction/" target="_blank">
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
          </div>
          <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
            <h3 class="mb-3 font-weight-bold dark-grey-text">
              <strong>Buscadores</strong>
            </h3>
            <p class="grey-text">Busque entre os milhares de arquivos presentes no meu servidor!</p><br/>
			<input type="text" class="w-50" name="b" id="b"/>
            <a href="<?= BASEURL ?>buscando.php" target="_blank" class="btn btn-primary btn-md">Buscar
              <i class="fas fa-search ml-2"></i>
            </a>
          </div>
        </div>
        <hr class="mb-5">
      </section>
    </div>
  </main>