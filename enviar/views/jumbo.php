<div class="jumbotron card card-image" style="background-image: linear-gradient(to top, #0099cc, #0086b3, #007399);">
  <div class="text-white text-center py-5 px-4">
    <div>
      <h2 class="card-title h1-responsive pt-3 mb-5 font-bold"><strong>Enviar os Arquivos</strong></h2>
      <p class="mx-5 mb-5">Nesta parte do site, é onde os arquivos são enviados. Preencha o formulário
	  abaixo para enviar o(s) arquivo(s) desejado(s).
      </p>
    </div>
  </div>
</div>