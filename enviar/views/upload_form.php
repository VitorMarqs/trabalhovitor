<?php ob_start(); ?>
    <div class="container">
        <div class="row mt-5 pt-5 mb-5">
            <div class="col-9">
                <form action="index.php" method="POST" enctype="multipart/form-data">
                    <h3 class="pt-2 mb-4">Selecione um arquivo para enviar</h3>
                    <input type="text" class="" name="nome" id="nome" placeholder="Nome do Arquivo">
                    </br>
                    </br>
                    <input type="text" class="" name="descricao" id="descricao" placeholder="Descricao">
                    </br>
                    </br>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Upload de arquivos</span>
                        </div>
                        <div class="custom-file">
                                <input type="file" name="arquivo" id="arquivo" />

                            <label class="custom-file-label" for="arquivo">Escolher arquivo</label>

                        </div>
                    </div>
                    <input class="btn mt-4 info-color #33b5e5"  type="submit" value="Enviar" name="submit" />
                    </form>
            </div>
        </div>
    </div>
<?php $upload_form = ob_get_clean(); ?>