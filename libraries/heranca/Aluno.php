<?php
include_once 'Pessoa.php';

class Aluno extends Pessoa {
    private $curso;
    private $escola;

    function __construct($nome, $curso){
        parent::__construct($nome);
        $this->curso = $curso;
    }

    public function pesquisar($tema){
        echo "Passar horas na Internet buscando $tema<br>";
    }

    public function estudar($tema, $disciplina){
        echo "Todo dia estudo $tema da disciplina $disciplina";
    }
    
    public function getNome(){
        return "Aluno ".parent::getNome();
    }
}