<?php
include_once 'Professor.php';
 
 class Diretor extends Professor{
     private $salario;
     private $horario;


     function __construct($nome, $salario, $horario){
        parent::__construct($nome, 'IFSP');
        $this->salario = $salario;
        $this->horario = $horario;
    }

    public function gerenciar($escola){
        echo "Diretor gerencia  $escola<br>";
    }

    public function liderar($escola, $funcionarios){
        echo "A partir de determinada posição na $escola o salarios dos $funcionarios são determinados<br>";
    }

    public function getNome(){
        return "Diretor ".parent::getNome();
    }
 } 

