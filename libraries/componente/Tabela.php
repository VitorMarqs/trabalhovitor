<?php


class Tabela{
    private $dados;
    private $rotulos;

    // dados são obrigatórios  e rótulos são opcionais
    function __construct($dados, $rotulos = null){
        $this->rotulos = $rotulos; 
        $this->dados = $dados;
    }
    /**
     * Gera o código html da tabela.
     */
    public function getHTML(){
         $html = '<table class="table"><tr>';
            foreach ($this->rotulos as $rotulo) {
                $html .= "<th>$rotulo</th>";
            }
            $html .= '</tr>';
        
            foreach ($this->dados as $row) {
                $html .= '<tr>';
                foreach ($row as $coluna) {
                    $html .= "<td>$coluna</td>";
                }
                $html .= $this->actionButtons($row['id']);
               
            }
            return $html .= '</table>';
    }
    private function actionButtons($id){
        $html = '<td><a href="'.BASEURL.'upload/editar.php?=id'.$id.'">';
        $html .= '<i class="far fa-edit mr-3 blue-text"></i></a>';
        $html .= '<a href="'.BASEURL.'upload/deletar.php?id='.$id.'">';
        $html .= '<i class="far fa-trash-alt red-text"></i></a></td>';
        return $html;      

    }
}